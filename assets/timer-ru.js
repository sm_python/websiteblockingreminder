var countDownDate = new Date("Feb 5, 2018 15:37:25").getTime();

var countdownfunction = setInterval(function () {

    var now = new Date().getTime();

    var distance = countDownDate - now;

    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    document.getElementById("timer").innerHTML = days + "д " + hours + "ч "
        + minutes + "м " + seconds + "с ";

    if (distance < 0) {
        clearInterval(countdownfunction);
        document.getElementById("timer").innerHTML = "Сайт заблокирован";
    }
}, 1000);

